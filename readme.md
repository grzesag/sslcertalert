# PHP script to monitor expiry of SSL certificate for a domain name(s)

## Description

This is a simple script to perform check of SSL certificate for list of
domain names.

For each domain name script fetches SSL certificate details such as expiry date. Next script performs
checks to see if SSL certificate for a domain name is valid, about to expire (next 30 days)
or expired.

Script sends report to specified email address(s):

* list of domain names that are about to expire or expired (if any), otherwise
* SSL certificate information for each domain name we monitor (if all SSL are valid)

## Requirements

* PHP 7.2+
* PHP curl extension

## How to use it?

* Edit ssl-domains.php file and add list of domain names witch you want to
  monitor
* Edit sslcertalert.php file and amend information that are used to send email:
  send to, send from, and subject
* Run the script

      php -f sslcertalert.php

* Use cron to schedule time and frequency when this script should run
