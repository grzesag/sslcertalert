<?php
/**
 * PHP script to monitor if web SSL certifcate is about to expire, valid or expired
 * 
 * Resources: https://php.net/curl
 *
 * Grzegorz Agacinski, webdevphere.com
 *
 * last modified: October, 2018 Grzegorz Agacinski
 */

include_once('ssl-domains.php'); // file with list of domain to check SSL web certificate expiry date

/**
 * Configuration settings
 */

// display PHP errors for this script
//ini_set('display_errors', 1);

// Email configuraiton

$mail_to 	    = ''; // email sent to
$mail_from 	  =  ''; // email sent from
$mail_subject	=  'SSL web certificate expiry report - '. date("d-m-Y", time()); // email subject


// =======================================================================================

// used to check SSL expire date
$date_today_ts         = mktime(); // Unix timestamp for a date, in seconds
$day_s                 = 24 * 60 * 60; // day in seconds
$alert_days_ts         = 30 * $day_s; // in seconds

$ssl_expired_message = array();             // to store information about SSL web certificate for domain
$ssl_valid            = 'valid';            // message for valid SSL
$ssl_expired          = 'expired';          // message for expired SSL
$ssl_about_to_expire  = 'about-to-expire';  // message for SSL that is about to expire

$ssl_details	      = array();	    // to store information for all processed URLs and SSLs

foreach ($urls as $url) {
        if ( preg_match('/^https/', $url) ) // perform SSL check only if we use HTTP protocol
        {

                $options = array (
                        CURLOPT_URL            => $url,
                        CURLOPT_RETURNTRANSFER => false, // don't return web page
                        CURLOPT_HEADER         => false,  // return headers
                        CURLOPT_CERTINFO       => true,  // output SSL certificate information
                        CURLOPT_VERBOSE        => true,  // output verbose information
                        CURLOPT_NOBODY         => true,  // exclude the body from the output
                        CURLOPT_USERAGENT      => 'SSL cert alert script - Department of Physics',
                );

          $ch = curl_init();
          curl_setopt_array($ch, $options);
          
         if (curl_exec($ch))
         {
            
	    $info = curl_getinfo($ch); // this is nested array

	    $cn          = explode("=",explode(",", $info['certinfo'][0]['Subject'])[4])[1];

            //$start_date  = explode(" ", $info['certinfo'][0]['Start date']);
	    //$expire_date = explode(" ", $info['certinfo'][0]['Expire date']);
	    
	    $start_date  = $info['certinfo'][0]['Start date']; // string
	    $expire_date = $info['certinfo'][0]['Expire date']; // string


	    //$expire_date_parts = explode("-", $expire_date[0]);
	    //$expire_date_ts = mktime(0, 0, 0, $expire_date_parts[1], $expire_date_parts[2], $expire_date_parts[0]);
	    	    
	    //$expire_date_ts = strtotime($expire_date[1] . ' '.  $expire_date[0] . ' ' . $expire_date[3]);
	    $expire_date_ts = strtotime($expire_date);

            // calculate for how many days SSL is valid for or when expired
            $interval =  $expire_date_ts - $date_today_ts;
            
	    $ssl_details[] = array(
		'url' => $info['url'],
		'cn' => $cn,
		//'start_date' => $start_date[1] . '-' . $start_date[0] . '-' . $start_date[3],
		'start_date' => date("d-m-Y", strtotime($start_date)),
	        //'expire_date' => $expire_date[1] . '-' . $expire_date[0] . '-' . $expire_date[3],
	    	'expire_date' => date("d-m-Y", $expire_date_ts),
		'valid_days' => round($interval/$day_s),
		);


            // find if current SSL id valid, about to expire or expired
            
            if ($interval > $alert_days_ts)
            {
                    $ssl_expired_message[][$ssl_valid] = 'SSL web certificate for the domain ' . $cn . ' is valid for ' . round($interval/$day_s) . ' days [Expiry day of the SSL certificate '. date("d-m-Y",$expire_date_ts).'].';
            }
            else if ($interval > 0 && $interval <= $alert_days_ts)
            {
                    $ssl_expired_message[][$ssl_about_to_expire] = 'SSL web certificate for the domain ' .$cn . ' is about to expire within ' . round($interval/$day_s) . ' days [Expiry day of the SSL certificate ' .date("d-m-Y", $expire_date_ts). '].';
            }
            else {
                    $ssl_expired_message[][$ssl_expired] = 'SSL web certificate for the domain ' .$cn . ' expired ' . round($interval/$day_s) . ' days ago [Expiry day of the SSL certificate ' .date("d-m-Y", $expire_date_ts). ']';            
            }

          }
	 else
	 {

	    $ssl_expired_message[][$ssl_expired]= 'SSL web certificate for the domain ' . $url . ' EXPIRED '; // we use url as cn is obtained from curl and it does files for expired certificates

            //echo '<h2>There is some problem with PHP curl module: ' .curl_error($ch). ' and domain: ' . $url.'</h2>';
            //echo 'To check if php5-curl is enabled run command: sudo php -m | grep curl<br/>';
            //echo 'You may need to install it e.g. apt-get install php-curl</br>';
          }

          curl_close($ch);

        }
}

$m = array(); // store just informaiton about SSL certificate that expired
foreach ( $ssl_expired_message as $line )
{
        $keys = array_keys($line);
        
        if ( $keys[0] == $ssl_valid ) continue;

        if ($keys[0] == $ssl_expired )
        {
                $color = 'red';
        }
        else if ( $keys[0] == $ssl_about_to_expire )
        {
                $color = 'red';
        }
        else 
        {
                $color = 'black';
        }

        $m[] = '<div style="color: '.$color.';">' . $line[$keys[0]].'</div>';
}
 
  $to 		= $mail_to;
  $subject 	= $mail_subject;
  $headers 	= 'MIME-Version: 1.0' . "\r\n";
  $headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $headers 	.= 'From: '. $mail_from  . "\r\n";

// send email only if we have information about web SSL certificate that expired
if (count($m) )
{
  $message = '
  <html>
  <head>
  <title>'.$subject.'</title>
  </head>
  <body>
  <h3>'.$subject.'</h3>
  ';
  foreach ($m as $value)
  {
          $message .= $value;
  }
  $message .='
  <body>
  </html>
  ';
  mail($to, $subject, $message, $headers);
}
else 
{
// display information about SSL certificates
$message_ssl_info = NULL;
$message_ssl_info = '<table>';
$message_ssl_info .= '<tr><th>URL</th><th>CN</th><th>Expire date</th><th>Valid (days)</th></tr>';
foreach ($ssl_details as $ssl_record) {
	$message_ssl_info .= '<tr><td>'. $ssl_record['url'].'</td><td>' .$ssl_record['cn'].'</td><td>'. $ssl_record['expire_date']. '</td><td>'.$ssl_record['valid_days'].'</td></tr>';
}
$message_ssl_info .= '</table>';
        $message = '
                <html>
                <head></head>
                <body>
                  <h3>At present all SSL web certificates are valid</h3>
		'. $message_ssl_info.'
                </body>
                </html> 
         ';
  mail($to, $subject, $message, $headers);
}

echo 'End of script: email was sent to: ' . $to;

